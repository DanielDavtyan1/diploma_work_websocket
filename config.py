import os.path

current_path = os.path.dirname(__file__)


class Config:
    JWT_SECRET_KEY = 'secret'
    SECRET_KEY = 'secret-key'
    REDIS_HOST = ''
    REDIS_PORT = ''


class DevConfig(Config):
    HOST = '127.0.0.1'
    PORT = 5000
    DEBUG = True
    PATH = os.path.join(current_path, 'users_data')
    SQLALCHEMY_DATABASE_URI = f"sqlite:///{PATH}"
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = '6379'
    KAFKA_HOST = '127.0.0.1:9092'
