import asyncio
import json
import time
import websockets
import cv2
from djitellopy import Tello
from kafka import KafkaProducer
import struct

from config import DevConfig

speed = (0, 0, 0, 0)
last_update = time.time()


# Initialize the Kafka producer
producer = KafkaProducer(bootstrap_servers=DevConfig.KAFKA_HOST)


async def video_stream_handler(tello, producer):
    # tello.streamon()
    # To be deleted later
    cap = cv2.VideoCapture(0)
    while True:
        # frame = tello.get_frame_read().frame
        # Convert frame to bytes
        ret, frame = cap.read()
        _, img_encoded = cv2.imencode('.jpg', frame)
        message = img_encoded.tobytes()
        # Push message to Kafka
        producer.send("Tello-edu-123456", message)
        await asyncio.sleep(0.01)


async def receive_speed(websocket, tello):
    global speed, last_update
    prev_speed = (0, 0, 0, 0)  # To track the previous speed command
    fail_safe_timeout = 3.0  # Adjust this as needed
    while True:
        current_time = time.time()
        if current_time - last_update > 0.01:
            if speed != prev_speed:
                print("Sending speed to drone", speed)
                # tello.send_rc_control(speed[0], speed[1], speed[2], speed[3])
                prev_speed = speed
            if current_time - last_update > fail_safe_timeout:
                # Fail-safe: if no new command is received within the timeout,
                # stop the drone
                speed = (0, 0, 0, 0)
                print("No command received within timeout, stopping drone")
            last_update = time.time()
        try:
            data = await asyncio.wait_for(websocket.recv(), timeout=0.01)
            data = json.loads(data)
            if data.get("command") == "SET_SPEED":
                rid = data.get("rid")
                new_speed = tuple(map(int, rid.split()))
                # Only update speed if it is not zero or if the new speed command is zero
                if new_speed != (0, 0, 0, 0) or speed != (0, 0, 0, 0):
                    speed = new_speed
                    last_update = current_time
                    print("Received speed from websocket", speed)
            if data.get("command") == "TAKE_OFF":
                # tello.takeoff()
                print("Taking off")
            if data.get("command") == "LAND":
                # tello.land()
                print("Landing")
        except (json.decoder.JSONDecodeError, asyncio.TimeoutError):
            pass


async def main():
    # tello = Tello()
    # tello.connect()
    # must be deleted later
    tello = None
    await asyncio.sleep(1)
    async with websockets.connect("ws://localhost:8765") as websocket:
        await websocket.send(json.dumps({"key": "constant key"}))
        await websocket.send("Tello-edu-123457")
        # Start the video stream handler
        video_stream_task = asyncio.create_task(video_stream_handler(tello, producer))
        await receive_speed(websocket, tello)
    # tello.land()
    # tello.end()
    # Cancel the video stream handler task
    # video_stream_task.cancel()


asyncio.run(main())
