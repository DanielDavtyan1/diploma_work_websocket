from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_cors import CORS
import redis

from config import DevConfig

app = Flask(__name__)
CORS(app)  # Add this line to enable CORS
app.config.from_object(DevConfig)
db = SQLAlchemy()
db.init_app(app)
jwt = JWTManager()
jwt.init_app(app)
redis_client = redis.Redis(host=DevConfig.REDIS_HOST, port=DevConfig.REDIS_PORT, db=0)


def create_db():
    with app.app_context():
        db.create_all()


from views import *

if __name__ == '__main__':
    create_db()
    app.run()
