from flask import request, jsonify
from flask_jwt_extended import create_access_token, create_refresh_token

from app import app, db, redis_client
from models import User
from helpers import generate_hash


@app.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        data = request.json
        username = data['username']
        password = data['password']
        user = User.query.filter_by(username=username, password=generate_hash(password)).first()
        if user:
            access_token = create_access_token(identity=username)
            redis_client.set(username, access_token, ex=3600)
            return jsonify(access_token=access_token), 200
        else:
            return jsonify(message='Invalid username or password'), 401
    return jsonify(message='Method Not Allowed'), 405


@app.route('/register', methods=['POST'])
def register_user():
    if request.method == 'POST':
        data = request.json
        firstname = data.get('firstname')
        lastname = data.get('lastname')
        username = data.get('username')
        email = data.get('email')
        phone = data.get('phone')
        password = data.get('password')
        if firstname and lastname and username and email and phone and password:
            if not User.query.filter_by(username=username).first():
                user = User(firstname=firstname,
                            lastname=lastname,
                            username=username,
                            email=email,
                            phone=phone,
                            password=generate_hash(password))
                db.session.add(user)
                db.session.commit()
                return jsonify(message='Successfully registered'), 200
        return jsonify(message='bad request'), 400
    return jsonify(message='Method Not Allowed'), 405
