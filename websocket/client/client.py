import asyncio
import json

from websocket.constants.constants import AVAILABLE_COMMANDS
from websocket.command.command import Command


class Client:
    def __init__(self, websocket, tello):
        self.tello = tello
        self.websocket = websocket

    async def send_command_response(self):
        while True:
            for key in self.tello.commands:
                command = self.tello.commands.get(key)
                if not command.response or command.end:
                    continue
                command.end = True
                string = json.dumps({"response": command.response, "rid": command.rid})
                await self.websocket.send(string)
            await asyncio.sleep(0.1)

    async def send_command_to_drone(self):
        while True:
            data = json.loads(await self.websocket.recv())
            rid = data["rid"]
            command = Command(data["command"], rid, data.get("info"))
            if command.command == "CLOSE":
                await self.websocket.close()
                break
            response_str = "unknown command"
            if command.command in AVAILABLE_COMMANDS:
                self.tello.commands[command.rid] = command
                response_str = "ok"
            response = {"response": response_str, "rid": rid}
            await self.websocket.send(json.dumps(response))
