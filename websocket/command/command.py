import json


class Command:
    def __init__(self, command, rid, info):
        self.command = command
        self.rid = rid
        self.info = info
        self.sent = False
        self.response = None

    def get_data(self):
        return json.dumps({"command": self.command, "rid": self.rid, "info": self.info})
