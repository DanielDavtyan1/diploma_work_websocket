import asyncio
import json


class TelloEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Tello):
            return obj.tello_id
        return super().default(obj)


class Tello:
    def __init__(self, websocket, drone_id):
        self.commands = {}
        self.websocket = websocket
        self.tello_id = drone_id

    def __str__(self):
        return self.tello_id

    async def send_command(self):
        while True:
            for key in self.commands:
                command = self.commands.get(key)
                if command.sent:
                    continue
                command.sent = True
                string = command.get_data()
                await self.websocket.send(string)
            await asyncio.sleep(0.1)

    async def get_command(self):
        while True:
            data = json.loads(await self.websocket.recv())
            rid = data["rid"]
            self.commands[rid].response = data["response"]
