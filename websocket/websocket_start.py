import asyncio
import json

import websockets
import redis
import kafka


from client.client import Client
from constants.constants import SDK_KEY
from tello.tello import Tello, TelloEncoder
from config import DevConfig

drones = []

redis_client = redis.Redis(host=DevConfig.REDIS_HOST, port=DevConfig.REDIS_PORT)
# kafka_admin_client = kafka.KafkaAdminClient(bootstrap_servers=DevConfig.KAFKA_HOST)


async def select_drone(websocket):
    while True:
        drone_id = await websocket.recv()
        drone_id = json.loads(drone_id)
        drone = drones[drone_id["index"]]
        del drones[drone_id["index"]]
        return drone


async def list_drone(websocket):
    await websocket.send(json.dumps(drones, cls=TelloEncoder))


async def login(websocket):
    # use jwt
    data = json.loads(await websocket.recv())
    key = data.get("key")
    if not key:
        raise ValueError("Key not provided")
    if key == SDK_KEY:
        return "drone"
    if redis_client.exists(key):
        return "client"


async def echo(websocket):
    user_type = await login(websocket)
    if user_type == "client":
        print("client !!!")
        await list_drone(websocket)
        tello = await select_drone(websocket)
        client = Client(websocket, tello)
        await websocket.send(json.dumps({"message": f"successfully connected to {client.tello.tello_id}"}))
        loop.create_task(client.send_command_to_drone())
        loop.create_task(client.send_command_response())

    elif user_type == "drone":
        print("tello !!!")
        drone_id = await websocket.recv()
        tello = Tello(websocket, drone_id)
        # try:
        #     # kafka_admin_client.create_topics([kafka.admin.NewTopic(name=drone_id, num_partitions=1, replication_factor=1)])
        # except kafka.errors.TopicAlreadyExistsError:
        #     pass
        drones.append(tello)
        await websocket.send("Your drone successfully added")
        loop.create_task(tello.get_command())
        loop.create_task(tello.send_command())
    await asyncio.sleep(11000000000)


async def main():
    async with websockets.serve(echo, "localhost", 8765):
        await asyncio.Future()  # run forever


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
# asyncio.run(main())
