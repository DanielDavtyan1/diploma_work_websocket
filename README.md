sudo docker run -d -p 6379:6379 --name my-redis-container redis


## For kafka
bin/zookeeper-server-start.sh config/zookeeper.properties

bin/kafka-server-start.sh config/server.properties
