import asyncio
import websockets
from aiokafka import AIOKafkaConsumer
import struct


async def consume(topic, websocket):
    consumer = AIOKafkaConsumer(
        topic,
        bootstrap_servers='localhost:9092',  # Replace with your Kafka server address
        group_id="my-group"
    )
    await consumer.start()
    try:
        async for msg in consumer:
            # Add a length prefix to each message before sending it to the WebSocket client
            length_prefix = struct.pack('!I', len(msg.value))
            await websocket.send(length_prefix + msg.value)
    finally:
        await consumer.stop()


async def websocket_server(websocket, path):
    topic = 'Tello-edu-123456'  # Replace with your topic name
    await consume(topic, websocket)


start_server = websockets.serve(websocket_server, "localhost", 5001)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
